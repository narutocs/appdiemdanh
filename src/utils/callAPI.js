require("dotenv").config();
import axios from "axios";
import * as API from "../constants/URL_API";

const callAPI = (endPoint,method,body,headerData)=>{
    const key = process.env.KEY;
    const value = process.env.VALUE;
    const header = {
        ...headerData,
        [key] : value,
        
    }
    return axios({
        url : `${API.URL_API}${endPoint}`,
        method : method,
        data : body,
        headers : header
    })
    
}
export default callAPI;