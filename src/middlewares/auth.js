require("dotenv").config();

import jwt from "jsonwebtoken";
import userModel from "../models/user.model";
import * as TypeRole from "../constants/TypeRole";

export {checkToken,checkTokenIsDoctor,checkTokenIsAdmin};


const checkToken = async(req,res,next)=>{
    try{
        const {authorization} = req.headers;
        if(!authorization){
            return res.status(400).send({
                message : "Chưa đăng nhập"
            })
        }
        const decode = await jwt.verify(authorization,process.env.JWT_KEY);
        
        if(userModel.findOne({_id:decode.id})){
            req.decode = decode;
            next();
        }else{
            return res.status(401).send({
                message : "Lỗi xác thực"
            });
        }
    }catch(e){
        next(e);
    }
}



const checkTokenIsAdmin = async(req,res,next)=>{
    try{
        const {authorization} = req.headers;
        if(!authorization){
            return res.status(400).send({
                message : "Chưa đăng nhập"
            })
        }
        const decode = await jwt.verify(authorization,process.env.JWT_KEY);
        const userFound = await userModel.findOne({_id:decode.id}).populate("idRole");
        if(userFound && userFound.idRole.name === TypeRole.SUPER_ADMIN){
            req.decode = decode;
            next();
        }else{
            return res.status(401).send({
                message : "Lỗi xác thực"
            });
        }
    }catch(e){  
        next(e);
    }
}


const checkTokenIsDoctor = async(req,res,next)=>{
    try{
        const {authorization} = req.headers;
        if(!authorization){
            return res.status(400).send({
                message : "Chưa đăng nhập"
            })
        }
        const decode = await jwt.verify(authorization,process.env.JWT_KEY);
        const userFound = await userModel.findOne({_id:decode.id}).populate("idRole");
        if(userFound && (userFound.idRole.name === TypeRole.BAC_SI || userFound.idRole.name === TypeRole.SUPER_ADMIN)){
            req.decode = decode;
            next();
        }else{
            return res.status(401).send({
                message : "Lỗi xác thực"
            });
        }
    }catch(e){
        next(e);
    }
}