import {Router} from "express";
import doctorRouter from "./doctor.router";
import userRouter from "./user.router";
import adminRouter from "./admin.router";
import roomRouter from "./room.router";
import caTrucRouter from "./caTruc.router";
import diemDanhRouter from "./diemDanh.router";

const router = Router();

router.use("/doctor",doctorRouter);

router.use("/user",userRouter);

router.use("/admin",adminRouter);

router.use("/room",roomRouter);

router.use("/shift",caTrucRouter);

router.use("/diemDanh",diemDanhRouter);

export default router;
