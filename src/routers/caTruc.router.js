import {Router} from "express";
import {createShift,deleteShift,updateShift,getListShift,getAShift} from "../controllers/caTruc.controller";
import {checkTokenIsDoctor} from "../middlewares/auth";
const router = Router();

router.get("/:id",checkTokenIsDoctor,getAShift);
router.get("/",checkTokenIsDoctor,getListShift);
router.put("/:id",checkTokenIsDoctor,updateShift);
router.delete("/:id",checkTokenIsDoctor,deleteShift);
router.post("/",checkTokenIsDoctor,createShift);

export default router;