import {Router} from "express";
import {register,registerPatient,getListPatient,addPatientToRoom,getInfoShift} from "../controllers/doctor.controller";
import uploadFile from "../libs/UploadFile";
import {checkTokenIsDoctor} from "../middlewares/auth";
const router = Router();

router.post("/register",uploadFile.array("avatars",6),register);
router.post("/register-patient",checkTokenIsDoctor,registerPatient);
router.get("/all-patient",checkTokenIsDoctor,getListPatient);
router.post("/add-patient-to-room/:id",checkTokenIsDoctor,addPatientToRoom);
router.get("/get-info-shift/:id",checkTokenIsDoctor,getInfoShift);

export default router;