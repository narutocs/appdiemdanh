import {Router} from "express";
import {createDiemDanh,endDiemDanh,history} from "../controllers/diemDanh.controller";
import {checkTokenIsDoctor} from "../middlewares/auth";
const router = Router();

router.post("/",checkTokenIsDoctor,createDiemDanh);

router.put("/",checkTokenIsDoctor,endDiemDanh);

router.get("/",checkTokenIsDoctor,history);

export default router;