import {Router} from "express";
import {getListUser,updateUser,deleteUser,addDoctorToShift, activeDoctor} from "../controllers/admin.controller";
import {checkTokenIsAdmin} from "../middlewares/auth";
const router = Router();

router.get("/all",checkTokenIsAdmin,getListUser);
router.put("/active-doctor/:id",checkTokenIsAdmin,activeDoctor);
router.put("/:id",checkTokenIsAdmin,updateUser);
router.delete("/:id",checkTokenIsAdmin,deleteUser);
router.post("/add-doctor-to-shift/:id",checkTokenIsAdmin,addDoctorToShift);


export default router;