import {Router} from "express";
import {login,uploadAvatars,getProfile,changePassword,diemDanh,generationCode,forgotPassword,checkCode} from "../controllers/user.controller";
import uploadFile from "../libs/UploadFile";
import {checkToken} from "../middlewares/auth";
const router = Router();

router.put("/change-password",checkToken,changePassword);
router.post("/login",login);
router.get("/",checkToken,getProfile);
router.post("/upload-avatars",checkToken,uploadFile.array("avatars",6),uploadAvatars);
router.post("/diem-danh",checkToken, uploadFile.single("image"),diemDanh);
router.post("/send-code",generationCode);
router.put("/forgot-password",forgotPassword);
router.post("/check-code",checkCode);

export default router;