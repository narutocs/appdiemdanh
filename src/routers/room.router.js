import {Router} from "express";
import {createRoom,deleteRoom,updateRoom,getListRoom,getARoom} from "../controllers/room.controller";
import {checkTokenIsDoctor, checkTokenIsAdmin} from "../middlewares/auth";
const router = Router();

router.get("/:id",checkTokenIsDoctor,getARoom);
router.get("/",checkTokenIsDoctor,getListRoom);
router.put("/:id",checkTokenIsAdmin,updateRoom);
router.delete("/:id",checkTokenIsAdmin,deleteRoom);
router.post("/",checkTokenIsAdmin,createRoom);

export default router;