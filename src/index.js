require("dotenv").config();

import express,{Router} from "express";
import http from "http";
import bodyParser from "body-parser";
import morgan from "morgan";
import connectDatabase from "./libs/ConnectDatabase";
import appRouter from "./routers/index";
import {createDefaultAdmin} from "./controllers/admin.controller";
import {createDefaultRole} from "./controllers/role.controller";
import {getIO} from "./controllers/user.controller";
import {getIODiemDanh} from "./controllers/diemDanh.controller";
import socketIO from "socket.io";
import cors from "cors";
import cron from "cron";
import {get_vn,get_all,get_global} from "./controllers/dataCOVID.controller";

const app = express();

const server = http.createServer(app);
const CronJob = cron.CronJob;
const io = socketIO(server);

// getIO(io);



app.use("/public",express.static("public"));
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(cors());


const PORT = process.env.PORT ;


const createDefault = async()=>{
    await connectDatabase();
    await createDefaultRole();
    await createDefaultAdmin();
    
   

}
createDefault();
// app.use((req,res,next)=>{
//     res.header("Access-Control-Allow-Origin","*");
//     res.header(
//         "Access-Control-Allow-Headers",
//         "Origin, X-Requested-With, Content-Type, Accept, Authorization"
//     );
//     if(req.method === "OPTIONS"){
//         res.header("Access-Control-Allow-Methods", "PUT, POST, DELETE");
//         return res.status(200).json({});
//     }
//     next();
// })

app.use("/api",appRouter);
let lsSocket = [];


io.on("connection",socket=>{

    console.log("user connect: ", socket.id);
    const jobSendData = new CronJob({
        cronTime : "*/59 * * * * *",
        onTick :async ()=>{

            socket.emit("data-vn",await get_vn());
        
        
            socket.emit("data-global",await get_global());
    
            socket.emit("data-all",await get_all());
            
            
        },
        start : true,
        timeZone : 'Asia/Ho_Chi_Minh'
    
    });
    lsSocket.push(socket);
    getIO(lsSocket);
   
    getIODiemDanh(lsSocket);

    socket.on("disconnect",()=>{
        let index = lsSocket.indexOf(socket);
        lsSocket.splice(index,1);
        console.log("client disconnect");
    });
})


app.use((error,req,res,next)=>{
    const status = error.status || 500;
    const message = error.message;
    console.log("message",message);
    res.status(status).send({
        message : message,
        statusCode : status
    });
});

app.use((req,res,next)=>{
    res.status(404).send({
        message : "PAGE NOT FOUND",
        statusCode : 404
    })
})

server.listen(PORT,()=>{
    console.log(`APP START AT PORT ${PORT}`)
})


