import {Schema, model} from "mongoose";

const roomSchema =  new Schema({
    _id : Schema.Types.ObjectId,
    name : {
        type : String,
        required: true,
        unique : true
    },
    address : {
        type : String,
        required : true
    },
    currentNumber : {
        type : Number,
        default: 0
    },
    maxNumber : {
        type : Number,
        required : true
    },
    idUser : [{
        type : Schema.Types.ObjectId,
        ref : "USER",
        default : ""
    }]
});

const roomModel = new model("ROOM",roomSchema);

export default roomModel;