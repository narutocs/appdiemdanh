import {Schema,model} from "mongoose";

const roleSchema = new Schema({
    _id : Schema.Types.ObjectId,
    name : String
});

const roleModel = new model("ROLE",roleSchema);

export default roleModel;