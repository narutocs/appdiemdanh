import {Schema,model} from "mongoose";

const userSchema =  new Schema({
    _id : Schema.Types.ObjectId,
    fullName : {
        type : String,
        required : true
    },
    email : {
        type : String,
        match : /\w+@([a-z]+\.[a-z]+)((.[a-z])*)/,
        required : true,
        unique : true
    },
    identityCard : {
        type : String,
        required : true,
        unique : true
    },
    avatars : [String],
    gender : {
        type : Number,
        default : 3
    },
    dateOfBirth : {
        type : String,
        match : /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/,
        required : true
    },
    phoneNumber : {
        type : String,
        required : true,
        unique : true
    },
    password : {
        type : String,
        required : true
    },
    isActive : {
        type : Boolean,
        default : 0
    },
    address : {
        type : String,
        required : true
    },
    idRole : {
        type : Schema.Types.ObjectId,
        ref : "ROLE",
        required : true
    },
    idCaTruc : {
        type : Schema.Types.ObjectId,
        ref : "CA_TRUC"
    },
    createAt : Number,
    gps : [Object],
    code : {
        type : Number,
        expires : '30s',
        default : null
    }
});

const userModel = new model("USER",userSchema);

export default userModel;