import {Schema, model} from "mongoose";

const thongBaoSchema = new Schema({
    _id : Schema.Types.ObjectId,
    message : String,
    status : {
        type : Boolean,
        default : false
    },
    title : String,
    idUser : {
        type : Schema.Types.ObjectId,
        ref : "USER"
    }
});

const thongBaoModel = new model("THONG_BAO",thongBaoSchema);

export default thongBaoModel;