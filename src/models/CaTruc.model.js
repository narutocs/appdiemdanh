import {Schema, model} from "mongoose";

const caTrucSchema = new Schema({
    _id : Schema.Types.ObjectId,
    startTime : {
        type :Number,
        required : true
    },
    endTime : {
        type: Number,
        required : true
    },
    idUser : [{
        type : Schema.Types.ObjectId,
        ref : "USER",
        default : ""
    }]
});

const caTrucModel = new model("CA_TRUC",caTrucSchema);

export default caTrucModel;