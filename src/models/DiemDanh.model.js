import {Schema,model} from "mongoose";

const diemDanhSchema = new Schema({
    _id : Schema.Types.ObjectId,
    startTime : Number,
    endTime : Number,
    idRoom : {
        type : Schema.Types.ObjectId,
        ref: "ROOM",
        required : true
    },
    listUser : [{
        type : Schema.Types.ObjectId,
        ref : "USER",
        default : ""
    }],
    idUser : {
        type : Schema.Types.ObjectId,
        ref : "USER",
        required : true
    } 
});

const diemDanhModel = new model("DIEM_DANH",diemDanhSchema);

export default diemDanhModel;