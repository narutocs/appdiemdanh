import diemDanhModel from "../models/DiemDanh.model";
import roomModel from "../models/room.model";
import userModel from "../models/user.model";
import caTrucModel from "../models/CaTruc.model";
import mongoose from "mongoose";

export {createDiemDanh,endDiemDanh, getIODiemDanh, history};


let lsSocket = [];

const getIODiemDanh = socket=>{
    lsSocket = socket;
}

const history = async(req,res,next)=>{
    try{
        const {idRoom, date} = req.query;
        let result = null;
        const lsDiemDanh = await diemDanhModel.find({idRoom}).populate({
            path : 'idUser',
            populate : {
                path : "idRole"
            }
        }).populate({
            path : "idRoom",
            populate : {
                path : "idUser"
            }
        }).populate("listUser");
        console.log("date",date);
        if(lsDiemDanh.length <= 0){
            let message = `Room chưa được điểm danh lần nào`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
        for(let i = 0;i<lsDiemDanh.length;i++){
            let dateOld = new Date(+lsDiemDanh[i].startTime);
          //  let month = dateOld.getMonth();
            let month = dateOld.getMonth() +1 ; // server
            let year = dateOld.getFullYear();
            let day = dateOld.getDate();
            let fullDate = `${day}/${month}/${year}`;
           
            let dateReceive = new Date(+date);
            console.log("fulldate : ",fullDate);
            console.log("date recevice",`${dateReceive.getDate()}/${dateReceive.getMonth()}/${dateReceive.getFullYear()}`)
            if(`${dateReceive.getDate()}/${dateReceive.getMonth()}/${dateReceive.getFullYear()}` === fullDate){
                
                result = lsDiemDanh[i];
                break;
            }
        }
        console.log("result: ",result)
        res.status(200).send({
            diemDanh : result
        })
    }catch(e){
        next(e);
    }
}

const createDiemDanh = async(req,res,next)=>{
    try{
        const {idRoom} = req.body;
        const roomFound = await roomModel.findOne({_id:idRoom}).populate({
            path : 'idUser',
            populate : {
                path : "idRole"
            }
        });
        if(!roomFound){
            let message = `idRoom not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
        if(roomFound.idUser.length <= 0){
            let message = `room is empty`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
       
    
        const idUser = req.decode.id;


        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth(); //January is 0!
        let yyyy = today.getFullYear();
        
        
     //   let timeStamp = new Date(yyyy,mm,dd,today.getHours(),today.getMinutes(),today.getSeconds()).getTime() ;
        let timeStamp = new Date(yyyy,mm,dd,today.getHours(),today.getMinutes(),today.getSeconds()).getTime() ;
        const roomDiemDanhFound = await diemDanhModel.findOne({idRoom : idRoom}).sort({startTime:-1});
        if(roomDiemDanhFound !== null){
            if(!roomDiemDanhFound.endTime){
                let message = `${roomFound.name} đang điểm danh`;
                let error = new Error(message);
                error.status = 400;
                throw error;
            }
            let dayFound = new Date(+roomDiemDanhFound.startTime);
            if(`${dd}/${mm + 1}/${yyyy}` === `${dayFound.getDate()}/${dayFound.getMonth() +1}/${dayFound.getFullYear()}`){
                let message = `${roomFound.name} đã điểm danh vào hôm nay vui lòng quay lại vào ngày mai`;
                let error = new Error(message);
                error.status = 400;
                throw error;
            }
        }
        
        

        const diemDanh = new diemDanhModel({
            _id : new mongoose.Types.ObjectId(),
            idUser,
            idRoom,
            startTime : timeStamp
        })
        await diemDanh.save();
        for(let i=0;i<lsSocket.length;i++){
            lsSocket[i].emit("start-check-in");
        }
        res.status(200).send({
            message : "Create diem danh success",
            users: roomFound.idUser
        })

    }catch(e){
        next(e);
    }
}

const endDiemDanh = async(req,res,next)=>{
    try{
        const {idRoom} = req.body;
        const roomFound = await roomModel.findOne({_id:idRoom}).populate({
            path : 'idUser',
            populate : {
                path : "idRole"
            }
        });
        if(!roomFound){
            let message = `idRoom not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }

        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth()).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();
        

        let timeStamp = new Date(yyyy,mm,dd,today.getHours(),today.getMinutes(),today.getSeconds()).getTime() ;
        const roomDiemDanhFound = await diemDanhModel.findOne({idRoom : idRoom}).sort({startTime:-1});
        if(!roomDiemDanhFound){
            let message = `${roomFound.name} chưa bắt đầu điểm danh`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
        if(roomDiemDanhFound && roomDiemDanhFound.endTime){
            let message = `${roomFound.name} đã kết thúc điểm danh`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
        const {nModified} = await diemDanhModel.updateOne({_id:roomDiemDanhFound._id},{endTime : timeStamp});
        if(nModified>0){
            res.status(200).send({
                message : "update success"
            })
        }else{
            let message = `update failed`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
    }catch(e){
        next(e);
    }
}