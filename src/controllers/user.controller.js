require("dotenv").config();

import jwt from "jsonwebtoken";
import userModel from "../models/user.model";
import roomModel from "../models/room.model";
import diemDanhModel from "../models/DiemDanh.model";
import bcrypt from "bcrypt";
import fs from "fs";
import {promisify} from "util";
import nodemailer from "nodemailer";
import * as TypeRole from "../constants/TypeRole";
import callAPI from "../utils/callAPI";
import {v4} from "uuid";
import cron from "cron";
import FormData from "form-data";
export {login,uploadAvatars,getProfile,changePassword,diemDanh,getIO,getListShift,generationCode,forgotPassword,checkCode};

const deleteFile = (files)=>{
    const unlink = promisify(fs.unlink);
    files.map(async(file,index)=>{
        return await unlink(file);
    })
}
const CronJob = cron.CronJob;
const transporter = nodemailer.createTransport({
    service : 'gmail',
    auth: {
        user: 'nhutcs123@gmail.com',
        pass: 'plzgtfcsdjugbrpx'
    },
    host : "smtp.gmail.com"
});

let lsSocket = [];
let location = [];
const getIO = socket=>{
    lsSocket = socket;
    for(let i=0;i<lsSocket.length;i++){
        lsSocket[i].on("user-location",async data=>{
            console.log("user-location: ",data);
            let isHave = -1;
            for(let j=0;j<location.length;j++){
                if(location[j].id === data.id){
                   isHave = j;
                }
            }
            if(isHave !== -1){
                location[isHave].location = data.location;
            }else{
                location.push(data);
            }
            await userModel.updateOne({_id:data.id}, {gps:[data.location]});
            
        });
    }
}
const jobSendDataLocation = new CronJob({
    cronTime : "*/59 * * * * *",
    onTick : ()=>{
        for(let i=0;i<lsSocket.length;i++){
            console.log("send-user-location: ",location);
            lsSocket[i].emit("data-location",location);
        }
    },
    start : true,
    timeZone : 'Asia/Ho_Chi_Minh'

});

const checkCode = async(req,res,next)=>{
    try{
        const {email,code} = req.body;
        const user = await userModel.findOne({ email });
        if(user){
            if(user.code !== null && +user.code == code){
                await userModel.updateOne({_id:user._id},{code:null});
                res.status(200).send({
                    message : "code đúng rồi nha"
                })
            }else{
                let error =  new Error();
                error.message = "Code không đúng nha";
                error.status = 404;
                throw error;
            }
        }else{
            let error =  new Error();
            error.message = "Email chưa được đăng ký";
            error.status = 404;
            throw error;
        }
    }catch(e){
        next(e);
    }
}

const forgotPassword = async(req,res,next)=>{
    try{
        const {email,password} = req.body;
        const hash = await bcrypt.hash(password,+process.env.SALT);
        const user = await userModel.findOne({ email })
        if(user){
            await userModel.updateOne({_id:user._id},{password:hash});
            res.status(200).send({
                message : "Thay đổi mật khẩu thành công"
            })
        }else{
            let error =  new Error();
            error.message = "Email chưa được đăng ký";
            error.status = 404;
            throw error;
        }
        
    }catch(e){
        next(e);
    }
}

const generationCode = async(req,res,next)=>{
    try{
        const {email} = req.body;
        console.log("mail: ",process.env.passMail)
        const code = Math.floor(Math.random() * (999999 - 100000 + 1) - 100000);
        const user = await userModel.findOne({ email })
      
        if (user) {
            await userModel.updateOne({_id:user._id},{code:code});
            const result = await transporter.sendMail({
                from: "Manager COVID",
                to: email,
                subject: "Mã xác thực",
                html: "<h1>" + "Mã xác thực của bạn là " + code + " mã sẽ hết hạn sau 5 phút</h1>"
            });
            const jobDeleCode = new CronJob({
                cronTime : '* 10 * * * *',
                onTick : async()=>{
                    console.log("delete run");
                    await userModel.updateOne({_id:user._id},{code:null});
                    console.log("delete done")
                },
                start : true,
                timeZone : 'Asia/Ho_Chi_Minh'
            
            });
            return res.status(200).send({
                message: "Kiểm tra email để xem mã xác thực",
                result : result
            }); 
        }else{
            let error =  new Error();
            error.message = "Email chưa được đăng ký";
            error.status = 404;
            throw error;
        }
    }catch(e){
        next(e);
    }
}

const getListShift = async(req,res,next)=>{
    try{
        const {idUser} = req.query;


    }catch(e){
        next(e);
    }
}

const diemDanh = async(req,res,next)=>{
   
    try{
        
        const {file} = req;
        if(!file){
            let message = `Chưa có image`;
            let error = new Error(message);
            error.status = 401;
            throw error;
        } 
        let formData = new FormData();
        const {id} = req.decode;
        const lsDiemDanh = await diemDanhModel.find({endTime : null});
        let index = -1;
        let currentDiemDanh = null;
        for(let i = 0;i<lsDiemDanh.length;i++){
            let {idRoom} = lsDiemDanh[i];
            let roomFound = await roomModel.findOne({_id : idRoom});
            index = roomFound.idUser.indexOf(id); 
            if(index !== -1){
                currentDiemDanh = lsDiemDanh[i];
                break;
            }
               
        }
        let indexFound = -1;
        if(currentDiemDanh){
            indexFound = currentDiemDanh.listUser.indexOf(id);
        }
       
        if(indexFound !== -1){
            let message = `Bạn đã điểm danh thành công rồi. Xin vui lòng đợt lượt điểm danh sau`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
        if(index === -1){
            let message = `Chưa tới giờ điểm danh vui lòng quay lại khi có thông báo từ nhân viên y tế`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }

        formData.append("image_file",fs.createReadStream(file.path));
        console.log("before");
        const result =await callAPI("/checkin/image","POST",formData,formData.getHeaders())
        console.log("after");
        const lsFaces = result.data.data.faces;
        let flag = false;
        
        for(let i=0;i<lsFaces.length;i++){
            if(lsFaces[i].user_name === id){

                flag = true;
                
                break;
            }
        }
        if(flag){
            await diemDanhModel.updateOne({_id : currentDiemDanh._id},{$push : {listUser:id}});
            for(let i=0;i<lsSocket.length;i++){
                lsSocket[i].emit("checkInDone",{id: id});
            }
            res.status(200).send({
                message : "Điểm danh thành công"
            })
        }else{
            let message = `Điểm danh thất bại`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }

    }catch(e){
        next(e);
    }
}


const changePassword = async(req,res,next)=>{
    try{
        const {id} = req.decode;
        const {newPassword,againNewPassword} = req.body;
        if(newPassword!==againNewPassword){
            let message = `again password and new password not same`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }else{
            const hash = await bcrypt.hash(newPassword,+process.env.SALT);
            const {nModified} = await userModel.updateOne({_id:id},{password:hash});
            if(nModified>0){
                res.status(200).send({
                    message : "update success"
                })
            }else{
                let message = `update failed`;
                let error = new Error(message);
                error.status = 400;
                throw error;
            }
           
        }
    }catch(e){
        next(e);
    }
}

const getProfile = async(req,res,next)=>{
    try{
        const {id,roleName} = req.decode;
      //  const userFound = await userModel.findOne({_id:id}).populate(["idRole","idCaTruc"]);
        const userFound = await userModel.findOne({_id:id}).populate(["idRole"]);
        // if(roleName === TypeRole.BENH_NHAN){

        // }
        res.status(200).send({
            user: userFound,
            statusCode: 200
        });
    }catch(e){
        next(e);
    }
}


const uploadAvatars = async(req,res,next)=>{
    try{
        const {files} = req;
        if(!files){
            let message = `Chưa có avatar`;
            let error = new Error(message);
            error.status = 401;
            throw error;
        }
        let formData = new FormData();
       
        const {id} = req.decode;
        const idUUID = v4();

        formData.append("id",idUUID);
        formData.append("name",id);
        for(let i =0;i<files.length;i++){
            formData.append("image_files",fs.createReadStream(files[i].path));
        }
    
        const avatars = files.map((item,index)=>{
            formData.append("image_files",fs.createReadStream(item.path));
            return `public/img/${item.originalname}`;

        })

      
        await userModel.updateOne({_id:id},{avatars:avatars});
    
        
        const result = await callAPI("/users","POST",formData,formData.getHeaders());

      

        if(result.data.error.code === 201){
            res.status(200).send({
                message : "Update image success",
                statusCode : 200
            })
        }else{
            let message = `error when upload image to deephub`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }

        

        
    }catch(e){
        next(e);
    }
}



const login = async(req,res,next)=>{
    try{
        const {identityCard,password} = req.body;
        const userFound = await userModel.findOne({identityCard}).populate("idRole");
        if(userFound){

            if(!userFound.isActive && userFound.idRole.name === TypeRole.BAC_SI){
                let message = `Tài khoản chưa được kích hoạt vui lòng liên hệ admin để được kích hoạt`;
                let error = new Error(message);
                error.status = 401;
                throw error;
            }

            if(!bcrypt.compareSync(password,userFound.password)){
                let message = `Lỗi xác thực`;
                let error = new Error(message);
                error.status = 401;
                throw error;
            }
            const body = {
                id:userFound._id,
                idRole:userFound.idRole,
                idCaTruc : userFound.idCaTruc,
                isActive: userFound.isActive,
                roleName : userFound.idRole.name
            };
            const token = await jwt.sign(body,process.env.JWT_KEY,{expiresIn : process.env.expiresIn});
            
            res.status(200).send({
                message : "Login success",
                token : token
            });
        }else{
            let message = `Lỗi xác thực`;
            let error = new Error(message);
            error.status = 401;
            throw error;
        }
    }catch(e){
        const {files} = req;
        if(files){
            const avatars = files.map((item,index)=>{
                return `public/img/${item.originalname}`;
            })
            deleteFile(avatars);
        }
        next(e);
    }
}

