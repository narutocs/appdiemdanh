import axios from "axios";

export {get_vn,get_global,get_all};

const get_vn = async()=>{
    try{
        const result = await axios({
            url : "https://ncovi.huynhhieu.com/live-api.php?code=vn",
            method: "GET"
        })
     
        return JSON.stringify(result.data);
    }catch(e){
        // console.log(e);
    }
}

const get_global = async()=>{
    try{
        const result = await axios({
            url : "https://ncovi.huynhhieu.com/api.php?code=global",
            method: "GET"
        })
        return JSON.stringify(result.data);
    }catch(e){
        // console.log(e);
    }
}

const get_all = async()=>{
    try{
        
        const result = await axios({
            url : "https://ncovi.huynhhieu.com/api.php?code=list&_=1590574649624",
            method: "GET"
        })
        return JSON.stringify(result.data);
    }catch(e){
        // console.log(e);
    }
}