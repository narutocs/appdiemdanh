require("dotenv").config();

import userModel from "../models/user.model";
import bcrypt from "bcrypt";
import fs from "fs";
import {promisify} from "util";
import mongoose from "mongoose";
import roomModel from "../models/room.model";
import roleModel from "../models/role.model";
import callAPI from "../utils/callAPI";
import {v4} from "uuid"
import FormData from "form-data"
import shiftModel from "../models/CaTruc.model";
import * as TypeRole from "../constants/TypeRole";
export { register , registerPatient, getListPatient,addPatientToRoom,getInfoShift};

const deleteFile = (files)=>{
    const unlink = promisify(fs.unlink);
    files.map(async(file,index)=>{
        return await unlink(file);
    })
}
const addPatientToRoom = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const {idRoom} = req.body;
        const userFound = await userModel.findOne({_id : id});
        const roomFound = await roomModel.findOne({_id : idRoom});
        const index=  roomFound.idUser.indexOf(id);
        if(index !== -1){
            let message = `patient added`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
        if(!userFound){
            let message = `patient not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
        if(!roomFound){
            let message = `room not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        };
        if(+roomFound.currentNumber+1 > +roomFound.maxNumber){
            let message = `room is full`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
        const {nModified} = await roomModel.updateOne({_id:idRoom},{$push: {idUser : id},currentNumber:+roomFound.currentNumber+1 });
        if(nModified>0){
            res.status(200).send({
                message : "add patient success"
            })
        }else{
            let message = `add patient failed`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
    }catch(e){
        next(e);
    }
}

const getListPatient = async(req,res,next)=>{
    try{
        const { page = 0, amount = 10, typesort = 1, search = "" } = req.query;
        const roleFound = await roleModel.findOne({name:TypeRole.BENH_NHAN});
        const total = await userModel.find({name : TypeRole.BENH_NHAN}).count();
        const users = await userModel.find({ idRole: roleFound._id,fullName: { $regex: search, $options: "$i" } }).limit(+amount)
        .skip(+amount * +page).sort({ fullName: typesort }).populate("idRole");
        res.status(200).send({
            users: users ,
            total: total,
            page: page,
            amount: amount,
            typesort: typesort,
            search: search
        });
    }catch(e){
        next(e);
    }
}

const registerPatient = async(req,res,next)=>{
    try{
        const user = req.body;
        const hash = await bcrypt.hash(123456 + "",+process.env.SALT);
        user.password = hash;
        const roleFound = await roleModel.findOne({name:TypeRole.BENH_NHAN});
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth(); //January is 0!
        let yyyy = today.getFullYear();
        
        
     //   let timeStamp = new Date(yyyy,mm,dd,today.getHours(),today.getMinutes(),today.getSeconds()).getTime() ;
        let timeStamp = new Date(yyyy,mm,dd,today.getHours(),today.getMinutes(),today.getSeconds()).getTime() ;
        const newUser = new userModel({
            _id : new mongoose.Types.ObjectId(),
            ...user,
            createAt :timeStamp,
            isActive : true,
            idRole : roleFound._id
        });
        await newUser.save();
        const {_doc} =  newUser;
        const dataRole = roleFound._doc
        res.status(200).send({
            message : "register thành công",
            result : {
                ..._doc,
                idRole : {
                    ...dataRole
                }
                
            }
        })
    }catch(e){
        next(e);
    }
}


const getInfoShift = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const lsShift = await shiftModel.find().populate({
            path : 'idUser',
            populate : {
                path : "idRole"
            }
        });
        
    //    const index = lsShift.idUser.indexOf(id);
        const lsShiftOfId = [];
        lsShift.map(shift=>{
            if(shift.idUser.length > 0){
                shift.idUser.map(user=>{
                    if(user._id == id)
                        lsShiftOfId.push(shift) ;
                })
            }
        })
        res.status(200).send({
            shift : lsShiftOfId
        })
    }catch(e){
        next(e);
    }
}

    const register = async(req,res,next)=>{
        try{
            
            const user = req.body;
            const {files} = req;
            let avatars = [];
            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth(); //January is 0!
            let yyyy = today.getFullYear();
            
            
         //   let timeStamp = new Date(yyyy,mm,dd,today.getHours(),today.getMinutes(),today.getSeconds()).getTime() ;
            let timeStamp = new Date(yyyy,mm,dd,today.getHours(),today.getMinutes(),today.getSeconds()).getTime() ;
            let newUser;


            if(files !== undefined && files.length > 0){
                
                avatars = files.map((item,index)=>{
                    return `public/img/${item.originalname}`;
                })
                const hash = await bcrypt.hash(user.password + "",+process.env.SALT);
                user.password = hash;
                const roleFound = await roleModel.findOne({name:TypeRole.BAC_SI});

                newUser = new userModel({
                    _id : new mongoose.Types.ObjectId(),
                    ...user,
                    createAt : timeStamp,
                    avatars,
                    idRole : roleFound._id
                });  
                const idUUID = v4();
                let formData = new FormData();
                
                formData.append("id",idUUID);
                
                formData.append("name",newUser._id + "");
                
                for(let i =0;i<files.length;i++){
                    formData.append("image_files",fs.createReadStream(files[i].path));
                }
            
            
                await callAPI("/users","POST",formData,formData.getHeaders());
            }else{
                const hash = await bcrypt.hash(user.password + "",+process.env.SALT);
                user.password = hash;
                const roleFound = await roleModel.findOne({name:TypeRole.BAC_SI});

                newUser = new userModel({
                    _id : new mongoose.Types.ObjectId(),
                    ...user,
                    createAt : timeStamp,
                    idRole : roleFound._id
                });
            }
            
            await newUser.save();
            res.status(200).send({
                message : "register thành công",
                result : newUser
            })

        }catch(e){

            const {files} = req;
            if(files){
                const avatars = files.map((item,index)=>{
                    return `public/img/${item.originalname}`;
                })
                deleteFile(avatars);
            }
            
            next(e);
        }
    }