require("dotenv").config();

import roleModel from "../models/role.model";
import * as TypeRole from "../constants/TypeRole";
import mongoose from "mongoose";
export {createDefaultRole};

const createDefaultRole =async (req,res,next)=>{
    try{
        const count = await roleModel.find().countDocuments();
        if(count === 0){
            const doctorRole = new roleModel({
                _id : new mongoose.Types.ObjectId(),
                name : TypeRole.BAC_SI
            });
            const benhNhanRole = new roleModel({
                _id : new mongoose.Types.ObjectId(),
                name : TypeRole.BENH_NHAN
            });
            const adminRole = new roleModel({
                _id : new mongoose.Types.ObjectId(),
                name : TypeRole.SUPER_ADMIN
            });
            await doctorRole.save();
            await benhNhanRole.save();
            await adminRole.save();
        }
        
    }catch(e){
        next(e);
    }
}