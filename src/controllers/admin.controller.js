require("dotenv").config();
import userModel from "../models/user.model";
import mongoose from "mongoose";
import roleModel from "../models/role.model";
import caTrucModel from "../models/CaTruc.model";
import * as TypeRole  from "../constants/TypeRole";
import bcrypt from "bcrypt";

export {getListUser,deleteUser,updateUser,createDefaultAdmin,addDoctorToShift,activeDoctor}


const activeDoctor = async(req,res,next)=>{
    try{
        const {id} = req.params;
        console.log("id ",id);
        if(!id){
            let message = `idDoctor is require`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
        const userFound = await userModel.findOne({_id:id}).populate("idRole");
        console.log("userfound: ",userFound);
        if(!userFound || userFound.idRole.name !== TypeRole.BAC_SI){
            let message = `Doctor not found`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }

        const {nModified} = await userModel.updateOne({_id:userFound._id},{isActive : !userFound.isActive});
        console.log("nmode",nModified)
         if(nModified>0){
            res.status(200).send({
                message : "update success"
            })
        }else{
            let message = `update failed`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
    }catch(e){
        next(e);
    }
}

const createDefaultAdmin = async(req,res,next)=>{
    try{
        const roleFound = await roleModel.findOne({name:TypeRole.SUPER_ADMIN});
        
        const userFound = await userModel.findOne({idRole : roleFound._id});
        if(!userFound){
            const hash = await bcrypt.hash("123456",+process.env.SALT);
            const superAdmin = new userModel({
                _id : new mongoose.Types.ObjectId(),
                fullName : "Hà Hữu Nhựt",
                email : "nhutcs123@gmail.com",
                identityCard : 123456789,
                password : hash,
                idRole : roleFound._id  ,
                phoneNumber : 84386741177 ,
                dateOfBirth : "12/06/1998",
                address : "Bình Dương",
                isActive : true 
            });
            await superAdmin.save();
        }
    }catch(e){
        next(e);
    }
}


const addDoctorToShift = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const {idShift} = req.body;
        const doctorFound = await userModel.findOne({_id:id});
        const shiftFound = await caTrucModel.findOne({_id:idShift});
        const index=  shiftFound.idUser.indexOf(id);
        if(index !== -1){
            let message = `patient added`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
        if(!doctorFound){
            let message = `doctor not found`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }
        if(!shiftFound){
            let message = `shift not found`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }

        const {nModified} = await caTrucModel.updateOne({_id:idShift},{$push: {idUser : id}});
        if(nModified>0){
            res.status(200).send({
                message : "add doctor success"
            })
        }else{
            let message = `add doctor failed`;
            let error = new Error(message);
            error.status = 400;
            throw error;
        }

    }catch(e){
        next(e);
    }
}

const getListUser = async(req,res,next)=>{
    try{
        const { page = 0, amount = 10, typesort = 1, search = "" } = req.query;
        const total = await userModel.find().count();
        const users = await userModel.find({fullName: { $regex: search, $options: "$i" } }).limit(+amount)
        .skip(+amount * +page).sort({ fullName: typesort }).populate("idRole");
        res.status(200).send({
            users: users ,
            total: total,
            page: page,
            amount: amount,
            typesort: typesort,
            search: search
        });
    }catch(e){
        next(e);
    }
}

const deleteUser = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const userFound = await userModel.findOne({_id:id});
        if(userFound){
            const {deletedCount} = await userModel.deleteOne({_id:id});
            if(deletedCount>0){
                res.status(200).send({
                    message : "delete success"
                })
            }else{
                let message = `delete failed`;
                let error = new Error(message);
                error.status = 400;
                throw error;
            }
        }else{
            let message = `user not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
    }catch(e){
        next(e);
    }
}

const updateUser = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const userFound = await userModel.findOne({_id:id});
        if(userFound){
            const user = req.body;
            const {nModified} = await userModel.updateOne({_id:id},user);
            if(nModified>0){
                res.status(200).send({
                    message : "update success"
                })
            }else{
                let message = `update failed`;
                let error = new Error(message);
                error.status = 400;
                throw error;
            }
        }else{
            let message = `user not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
    }catch(e){
        next(e);
    }
}