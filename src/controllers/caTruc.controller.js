require("dotenv").config();

import caTrucModel from "../models/CaTruc.model";
import mongoose from "mongoose";

export {createShift,updateShift,deleteShift,getListShift,getAShift}

const getAShift = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const caTrucFound = await caTrucModel.findOne({_id:id});
        if(caTrucFound){
            res.status(200).send({
                shift : caTrucFound
            })
        }else{
            let message = `shift not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
    }catch(e){
        next(e)
    }
}

const createShift = async(req,res,next)=>{
    try{
        const caTruc = req.body;

        if(+caTruc.startTime >= +caTruc.endTime){
            let message = `start time can not bigger end time`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
        const newCaTruc = new caTrucModel({
            _id : new mongoose.Types.ObjectId(),
            ...caTruc
        });

        await newCaTruc.save();

        res.status(200).send({
            message : "create Shift success",
            statusCode : 200
        })
    }catch(e){
        next(e);
    }
}

const getListShift = async(req,res,next)=>{
    try{
        const caTruc = await caTrucModel.find().populate("idUser");
        res.status(200).send({
            caTruc: caTruc 
        });
    }catch(e){
        next(e);
    }
}

const updateShift = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const caTrucFound = await caTrucModel.findOne({_id:id});
        if(caTrucFound){
            const caTruc = req.body;
            if(caTruc.startTime && caTruc.endTime){
                if(+caTruc.startTime >= +caTruc.endTime){
                    let message = `start time can not bigger end time`;
                    let error = new Error(message);
                    error.status = 400;
                    throw error;
                }
            }
            else if(+caTruc.startTime >= +caTrucFound.endTime || +caTrucFound.startTime >= +caTruc.endTime){
                let message = `start time can not bigger end time`;
                let error = new Error(message);
                error.status = 400;
                throw error;
            }
            const {nModified} = await caTrucModel.updateOne({_id:id},caTruc);
            if(nModified>0){
                res.status(200).send({
                    message : "update success"
                })
            }else{
                let message = `update failed`;
                let error = new Error(message);
                error.status = 400;
                throw error;
            }
        }else{
            let message = `Shift not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
    }catch(e){
        next(e);
    }
}

const deleteShift = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const caTrucFound = await caTrucModel.findOne({_id:id});
        if(caTrucFound){
            const {deletedCount} = await caTrucModel.deleteOne({_id:id});
            if(deletedCount>0){
                res.status(200).send({
                    message : "delete success"
                })
            }else{
                let message = `delete failed`;
                let error = new Error(message);
                error.status = 400;
                throw error;
            }
        }else{
            let message = `Shift not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
    }catch(e){
        next(e);
    }
}