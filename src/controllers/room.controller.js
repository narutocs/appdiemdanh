require("dotenv").config();

import roomModel from "../models/room.model";
import mongoose from "mongoose";

export {createRoom,getListRoom,updateRoom,deleteRoom,getARoom}

const createRoom = async(req,res,next)=>{
    try{
        const room = req.body;

        const newRoom = new roomModel({
            _id : new mongoose.Types.ObjectId(),
            ...room
        });

        await newRoom.save();

        res.status(200).send({
            message : "create room success",
            statusCode : 200
        })
    }catch(e){
        next(e);
    }
}

const getListRoom = async(req,res,next)=>{
    try{
        const { page = 0, amount = 10, typesort = 1, search = "" } = req.query;
        const total = await roomModel.find().count();
        const room = await roomModel.find({name: { $regex: search, $options: "$i" } }).limit(+amount)
        .skip(+amount * +page).sort({ name: typesort }).populate({
            path : 'idUser',
            populate : {
                path : "idRole"
            }
        });
        res.status(200).send({
            room: room ,
            total: total,
            page: page,
            amount: amount,
            typesort: typesort,
            search: search
        });
    }catch(e){
        next(e);
    }
}

const updateRoom = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const roomFound = await roomModel.findOne({_id:id});
        if(roomFound){
            const room = req.body;
            const {nModified} = await roomModel.updateOne({_id:id},room);
            if(nModified>0){
                res.status(200).send({
                    message : "update success"
                })
            }else{
                let message = `update failed`;
                let error = new Error(message);
                error.status = 400;
                throw error;
            }
        }else{
            let message = `room not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
    }catch(e){
        next(e);
    }
}

const deleteRoom = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const roomFound = await roomModel.findOne({_id:id});
        if(roomFound){
            const {deletedCount} = await roomModel.deleteOne({_id:id});
            if(deletedCount>0){
                res.status(200).send({
                    message : "delete success"
                })
            }else{
                let message = `delete failed`;
                let error = new Error(message);
                error.status = 400;
                throw error;
            }
        }else{
            let message = `room not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
    }catch(e){
        next(e);
    }
}

const getARoom = async(req,res,next)=>{
    try{
        const {id} = req.params;
        const roomFound = await roomModel.findOne({_id:id});
        if(roomFound){
            res.status(200).send({
                room : roomFound
            })
        }else{
            let message = `room not found`;
            let error = new Error(message);
            error.status = 404;
            throw error;
        }
    }catch(e){
        next(e)
    }
}